#!/bin/bash
set -e
python /opt/rucio-jl-docker/configure.py
exec "$@"
