# ESCAPE Data Lake-as-a-Service Singleuser Python 3.9.13 Base Image

This image is derived from [datalake-singleuser](https://gitlab.cern.ch/escape-wp2/docker-images/-/tree/master/datalake-singleuser).

It contains the same modifications (`rucio-jupyterlab`, `rucio clients`, `python-gfal2`, ESCAPE `certs` and `VOMSes` and `swanoauthrenew`) except that the python version is not downgraded to 3.8. The python version is 3.9.13.

Thus, please do not use this image as a `BASE` layer if you want to extend the image with PyROOT or ROOT dependencies. In this case use [datalake-singleuser](https://gitlab.cern.ch/escape-wp2/docker-images/-/tree/master/datalake-singleuser).


## Using the image as a standalone instance

If you want to take advantage of the extension's capability in your own machine, you can do a simple Docker run:

```sh
docker run -p 8888:8888 gitlab-registry.cern.ch/escape-wp2/docker-images/datalake-singleuser-py39:latest
```

## Extending the image

Please have a look to the original [documentation](https://gitlab.cern.ch/escape-wp2/docker-images/-/blob/master/datalake-singleuser/README.md)
